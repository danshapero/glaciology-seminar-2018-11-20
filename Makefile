
all: edges.tif

%.TIF:
	python3 fetch.py

petermann_ps.tif: LC08_L1TP_041001_20180831_20180912_01_T1_B8.TIF
	rio pansharpen -j 4 LC08_L1TP_041001_20180831_20180912_01_T1_B{8,4,3,2}.TIF \
	    petermann_ps.tif

petermann.tif: petermann_ps.tif
	rio color -j 4 petermann_ps.tif petermann.tif sigmoidal rgb 10 0.13

edges.tif: petermann.tif
	python3 edge_detection.py
