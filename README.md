The package `rio-pansharpen` hasn't been updated in a while and has fallen out of sync with the main rasterio package.
I made a fix for this, so to install my version you can do the following:

    git clone https://github.com/danshapero/rio-pansharpen.git -b window-fix
    cd rio-pansharpen
    pip3 install -e . --user

