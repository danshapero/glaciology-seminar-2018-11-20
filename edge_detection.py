import numpy as np
import cv2 as opencv
import rasterio

with rasterio.open("petermann.tif", 'r') as img_source:
    profile = img_source.profile
    img = img_source.read()

# rasterio and OpenCV have different conventions about how to store the bands
# of a color image, so we have to rearrange things.
nb, ny, nx = img.shape
image = np.zeros((ny, nx, 3), dtype=img.dtype)
for band in range(3):
    image[:, :, band] = img[band, :, :]

edges = opencv.Canny(image, 100, 200)

profile.update(count=1)
with rasterio.open("edges.tif", 'w', **profile) as destination:
    destination.write(edges, 1)
