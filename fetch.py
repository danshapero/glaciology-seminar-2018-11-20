import os
import requests
import tqdm

base = "https://landsat-pds.s3.amazonaws.com/c1/L8"
path = 41
row = 1
start_date = "20180831"
end_date = "20180912"

stem = "LC08_L1TP_{path:03d}{row:03d}_{start_date}_{end_date}_01_T1".format(
       path=path, row=row, start_date=start_date, end_date=end_date)
urls = ["{base}/{path:03d}/{row:03d}/{stem}/{stem}_B{band}.TIF".format(
        base=base, path=path, row=row, stem=stem, band=band)
        for band in [8, 4, 3, 2, "QA"]]

for url in urls:
    response = requests.get(url, stream=True)
    size = int(response.headers.get('content-length'))
    with open(os.path.basename(url), 'wb') as image_file:
        for data in tqdm.tqdm(response.iter_content(chunk_size=1024),
                              total=int(size/1024), unit='KiB'):
            image_file.write(data)
